<?php
/**
 * Created by PhpStorm.
 * User: engrs
 * Date: 3/18/2019
 * Time: 11:16 PM
 */

require_once 'Core.php';

class Product extends Core {

    public function read() {

        $sql = "SELECT  c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created FROM `products` AS p LEFT JOIN categories AS c ON p.category_id = c.id ORDER BY p.created DESC";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt;
    }
}