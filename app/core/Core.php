<?php
/**
 * Created by PhpStorm.
 * User: engrs
 * Date: 3/18/2019
 * Time: 11:22 PM
 */

require_once '../db/Config.php';
$database = new Config();
$db_config = $database->getConfig();

class Core {

    protected $db;

    public function __construct() {
        global $db_config;
        $this->db = $db_config;
    }
}