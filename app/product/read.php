<?php
/**
 * Created by PhpStorm.
 * User: engrs
 * Date: 3/18/2019
 * Time: 11:14 PM
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

require_once '../core/Product.php';

$product = new Product();
$res = $product->read();

if ($res->rowCount() > 0) {

    $product_arr = array();
    $product_arr['records'] = array();

    foreach ($res->fetchAll(PDO::FETCH_OBJ) as $row) {

        $product_item = array(
            'id' => $row->id,
            'name' => $row->name,
            'description' => html_entity_decode($row->description),
            'price' => $row->price,
            'category_id' => $row->category_id,
            'category_name' => $row->category_name
        );

        array_push($product_arr['records'], $product_item);
    }

    http_response_code(200);

    echo json_encode($product_arr);
} else {

    http_response_code(404);

    echo json_encode(
        array('message' => 'No products found.')
    );
}

